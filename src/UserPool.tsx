import { CognitoUserPool } from "amazon-cognito-identity-js";

const poolData = {
    UserPoolId: "eu-central-1_FEnKNmRmf",
    ClientId: "3v84irtsj9ouoe0qohkf9f3hps"
}

export default new CognitoUserPool(poolData);